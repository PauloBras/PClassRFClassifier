# ----------------------------------------------------------------------------
# Random Forest classifier using unsupervised learning (GMM labels)
# ----------------------------------------------------------------------------

from __future__ import print_function

import sys
import datetime
import random

from sklearn import datasets
from sklearn import metrics
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier

from sklearn.impute import SimpleImputer
from sklearn.inspection import permutation_importance
from sklearn.compose import ColumnTransformer
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import OneHotEncoder

from math import *
import matplotlib
from matplotlib.colors import LogNorm
matplotlib.use('Agg')
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
from matplotlib import rc

font = {'family' : 'DejaVu Serif',
        'weight' : 'normal',
        'size'   : 12}

rc('font', **font)

import time

import pickle

start_time = time.time()

if len(sys.argv)!=4 :
    if len(sys.argv)==2 :
        if sys.argv[1] == 'man' : 
            print('USAGE: python [script_name.py] - runs script with default settings')
            print('       python [script_name.py] [int]n_trees [int OR None]max_depth [int]min_samples_split')
            sys.exit('Exiting.......')
        else :
            print('####### ERROR ####### Expected argv to be "man"')
            sys.exit('Exiting.......')
    elif len(sys.argv)==1 :
        n_trees = 101 # 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101, 103, 107, 109, 113, 127, 131 151 181 191 313 353 373 383 727 757 787 797 919 929
        max_depth = None
        max_depth_2 = 0
        min_samples_split = 2
        print('n_trees           = {}'.format(n_trees))
        print('max_depth         = {}'.format(max_depth))
        print('min_samples_split = {}'.format(min_samples_split))
    else :
        print('WARNING: Use "python [script_name.py] man" to chack usage')
        print('####### ERROR ####### Expected 3 argv, {} given'.format(len(sys.argv)-1))
        sys.exit('Exiting........')
else :
    try :
        n_trees = int(sys.argv[1])
        max_depth = int(sys.argv[2])
        max_depth_2 = int(sys.argv[2])
        min_samples_split = int(sys.argv[3])
        if n_trees<5 or n_trees>300 :
            print('WARNING! number os estimators too low/high: setting to default of 101')
            n_trees = 101
        elif  max_depth<=0 :
            print('WARNING! depth of each estimator must be positive: setting to default of None')
            max_depth = None
        elif min_samples_split<2 :
            print('WARNING! min samples to split is 2: setting to default of 2')
            min_samples_split = 2
        print('n_trees           = {}'.format(n_trees))
        print('max_depth         = {}'.format(max_depth))
        print('min_samples_split = {}'.format(min_samples_split))
    except :
        print('####### ERROR ####### Wrong input format!! Should be: [int]n_trees [int OR None]max_depth [int]min_samples_split')
        sys.exit('Exiting.......')


filename = 'file.txt'
filename_gmm_results = 'GMM_fixedBase_67b.txt'
output_name = 'RF_GMM_{}trees_d{}s{}'.format(n_trees,max_depth_2,min_samples_split)
title_name = output_name

model_output_name = output_name+'.sav'

print('Initiating RF+GMM exercise. NAME: {}\nusing {} estimators (trees)'.format(output_name,n_trees))

plotting = False

lims_pApL90 =    (np.linspace(-4.5,7.5,150),np.linspace(1.0,6.0,150))
lims_pApF50 =    (np.linspace(-4.5,7.5,150),np.linspace(-0.2,1.5,150))
lims_pApF50_l =  (np.linspace(-4.5,7.5,150),np.linspace(-5.0,2.0,150))
lims_pApF100 =   (np.linspace(-4.5,7.5,150),np.linspace(-0.2,2.0,150))
lims_pApH =      (np.linspace(-4.5,7.5,150),np.linspace(-3.0,4.0,150))
lims_TBApA =     (np.linspace(-1.2,1.2,150),np.linspace(-4.5,7.5,150))
lims_TBApF50 =   (np.linspace(-1.2,1.2,150),np.linspace(-0.2,1.5,150))
lims_TBApF50_l = (np.linspace(-1.2,1.2,150),np.linspace(-5.0,2.0,150))


print('--------------------------------------------------------------------------------')
print('Importing data from: {}'.format(filename))
print('--------------------------------------------------------------------------------')

data = np.genfromtxt(filename, delimiter='\t', dtype=None, encoding=None, names=('pulseClass', 'pA', 'pF50', 'TBA', 'pH', 'pL', 'pL90', 'aft5', 'aft25', 'aft50', 'aft75', 'aft95', 'pA100', 'pA200', 'pA500', 'pA1k', 'pA2k', 'pA5k', 'pHT', 'pRMSW', 'coincidence', 'rawFileN', 'rqFileN', 'rawEventID', 'rqEventID', 'pulseID'))

data['pRMSW'][data['pRMSW']==0]=1

data=pd.DataFrame({
    'pA':data['pA'],
    'pF50':data['pF50'],
    'pF100':(data['pA100']/data['pA']),
    'pF200':(data['pA200']/data['pA']),
    'pF1k':(data['pA1k']/data['pA']),
    'TBA':data['TBA'],
    'pL90':(data['aft95']-data['aft5']),
    'pH':data['pH'],
    'pHTL':(data['pHT']/data['pL']),
    'pRMSW':data['pRMSW'],
    'class':data['pulseClass']
})
lenData = len(data)
LZ_classes = data['class']

#----------------------------------------- Extracting the classes of each pulse and changing Other=0 to Other=4
classes_index = np.unique(np.array(data['class']))
print('Class labels: {}'.format(classes_index))
num_classes = 4 #[s1, s2, se, other]
if len(classes_index)!=num_classes :
    print("Class number mismatch....")

print('--------------------------------------------------------------------------------')
print('Importing data from: {}'.format(filename))
print('--------------------------------------------------------------------------------')
gmm_results = np.loadtxt(filename_gmm_results, delimiter='\t')

gmm_bases = gmm_results[:,0]
if len(gmm_bases)!=lenData :
    print("ERROR!!! GMM data size not consistent with data size!! EXITING...")
    quit()

# mapping the GMM bases to observed classes (from averaged LZ classifiaction and handscanning)
gmm_classes = np.copy(gmm_bases)
gmm_classes[gmm_classes==0]=3
gmm_classes[gmm_classes==1]=4
gmm_classes[gmm_classes==2]=4
gmm_classes[gmm_classes==3]=2
gmm_classes[gmm_classes==4]=4
gmm_classes[gmm_classes==5]=4
gmm_classes[gmm_classes==6]=2
gmm_classes[gmm_classes==7]=3
gmm_classes[gmm_classes==8]=1
gmm_classes[gmm_classes==9]=4
gmm_classes[gmm_classes==10]=1
gmm_classes[gmm_classes==11]=1
gmm_classes[gmm_classes==12]=2
gmm_classes[gmm_classes==13]=4
gmm_classes[gmm_classes==14]=4
gmm_classes[gmm_classes==15]=4
gmm_classes[gmm_classes==16]=1
gmm_classes[gmm_classes==17]=4
gmm_classes[gmm_classes==18]=3
gmm_classes[gmm_classes==19]=2
gmm_classes[gmm_classes==20]=4
gmm_classes[gmm_classes==21]=4
gmm_classes[gmm_classes==22]=1
gmm_classes[gmm_classes==23]=2
gmm_classes[gmm_classes==24]=1
gmm_classes[gmm_classes==25]=1
gmm_classes[gmm_classes==26]=3
gmm_classes[gmm_classes==27]=2
gmm_classes[gmm_classes==28]=2
gmm_classes[gmm_classes==29]=2
gmm_classes[gmm_classes==30]=4
gmm_classes[gmm_classes==31]=2
gmm_classes[gmm_classes==32]=2
gmm_classes[gmm_classes==33]=2
gmm_classes[gmm_classes==34]=1
gmm_classes[gmm_classes==35]=4
gmm_classes[gmm_classes==36]=4
gmm_classes[gmm_classes==37]=4
gmm_classes[gmm_classes==38]=1
gmm_classes[gmm_classes==39]=4
gmm_classes[gmm_classes==40]=4
gmm_classes[gmm_classes==41]=2
gmm_classes[gmm_classes==42]=4
gmm_classes[gmm_classes==43]=3
gmm_classes[gmm_classes==44]=4
gmm_classes[gmm_classes==45]=1
gmm_classes[gmm_classes==46]=3
gmm_classes[gmm_classes==47]=4
gmm_classes[gmm_classes==48]=4
gmm_classes[gmm_classes==49]=2
gmm_classes[gmm_classes==50]=1
gmm_classes[gmm_classes==51]=3
gmm_classes[gmm_classes==52]=2
gmm_classes[gmm_classes==53]=4
gmm_classes[gmm_classes==54]=4
gmm_classes[gmm_classes==55]=2
gmm_classes[gmm_classes==56]=2
gmm_classes[gmm_classes==57]=2
gmm_classes[gmm_classes==58]=1
gmm_classes[gmm_classes==59]=2
gmm_classes[gmm_classes==60]=1
gmm_classes[gmm_classes==61]=4
gmm_classes[gmm_classes==62]=1
gmm_classes[gmm_classes==63]=1
gmm_classes[gmm_classes==64]=2
gmm_classes[gmm_classes==65]=4
gmm_classes[gmm_classes==66]=1

print('---------------------------------------------------------------------------------------------')
print('--------------------------------- DATA FORMATING COMPLETED ----------------------------------')
print('---------------------------------------------------------------------------------------------')

index = ['pA','pF50','pF100','pF200','pF1k','TBA','pL90','pH','pHTL','pRMSW']
X_train=data[['pA','pF50','pF100','pF200','pF1k','TBA','pL90','pH','pHTL','pRMSW']] # Features
y_train=gmm_classes  # GMM Labels

# Split dataset into training set and test set
X_train, X_test, y_train, y_test, LZ_train, LZ_test = train_test_split(X_train, y_train, LZ_classes, test_size=0.2, random_state=33) # 80% training and 20% test

#---------------------------------------------------- building the Classifier
RndForestPC=RandomForestClassifier(n_estimators=n_trees,max_depth=max_depth,min_samples_split=min_samples_split,verbose=2) 

#---------------------------------------------------- Training
RndForestPC.fit(X_train,y_train)

#---------------------------------------------------- Evaluate test set
y_pred=RndForestPC.predict(X_test)
print("Completed predicting test data at %s seconds" % (time.time() - start_time))

#---------------------------------------------------- Model Accuracy
val_acc = metrics.accuracy_score(y_test, y_pred)
print("Validation accuracy: ",val_acc)

print("---------------------------------- Confusion matrix -------------------------------")
print("------y_pred ({} - {})".format(type(y_pred),len(y_pred)))
matConf = pd.crosstab(y_test, y_pred, rownames=['Actual Species'], colnames=['Predicted Species'])
print(matConf)

#---------------------------------------------------- Save model to file if accuracy is above previously established/benchmarked value
if val_acc>0.993 :
    print('---------------------------------------------------------------------------------------------')
    print('validation accuracy is: {} - saving model'.format(val_acc))
    pickle.dump(RndForestPC, open(model_output_name, 'wb'))


print('---------------------------------------------------------------------------------------------')
print("------------------------------------ Feature Importance map ---------------------------------")
feature_imp = pd.Series(RndForestPC.feature_importances_,index=index).sort_values(ascending=False)
print(feature_imp)
print(feature_imp.values)
print(feature_imp.index)

plt.figure(figsize=(8, 6))
g = sns.barplot(x=feature_imp.index,y=feature_imp,palette="rocket")
#plt.xticks(rotation=45)
plt.ylabel('Feature Importance Score',fontsize=16)
plt.xlabel('Features',fontsize=16)
##plt.title("Variable importance (GINI)")
#plt.legend()
plt.savefig('IMAGES/'+title_name+'strength_params_fullClassifier_MDC3.png', bbox_inches = 'tight',pad_inches = 0.2)
plt.clf()

print("Completed plotting feature importance at %s seconds" % (time.time() - start_time))

print('---------------------------------------------------------------------------------------------')
print("------------------------------------ Permutation Importance ---------------------------------")
result_permImp = permutation_importance(RndForestPC, X_test, y_test, n_repeats=50, random_state=42, n_jobs=4)
sorted_idx = result_permImp.importances_mean.argsort()[::-1]

plt.figure(figsize=(8, 6))
g = sns.barplot(x=X_test.columns[sorted_idx],y=result_permImp.importances_mean[sorted_idx],palette="rocket")
plt.ylabel('Permutation Importance Score',fontsize=16)
plt.xlabel('Features',fontsize=16)
plt.savefig('IMAGES/'+title_name+'_permutation_test_fullClassifier.png', bbox_inches = 'tight',pad_inches = 0.2)
plt.clf()

result_permImp = permutation_importance(RndForestPC, X_train, y_train, n_repeats=50, random_state=42, n_jobs=4)
sorted_idx = result_permImp.importances_mean.argsort()[::-1]

plt.figure(figsize=(8, 6))
g = sns.barplot(x=X_test.columns[sorted_idx],y=result_permImp.importances_mean[sorted_idx],palette="rocket")
plt.ylabel('Permutation Importance Score',fontsize=16)
plt.xlabel('Features',fontsize=16)
plt.savefig('IMAGES/'+title_name+'_permutation_train_fullClassifier.png', bbox_inches = 'tight',pad_inches = 0.2)
plt.clf()

print('---------------------------------------------------------------------------------------------')
print('-------------------------------------- COMPLETED --------------------------------------------')
print('---------------------------------------------------------------------------------------------')

data = []
X_train = []
X_test = []
y_train = []
y_test = []
y_pred = []
y_train_s1 = []
y_train_s2 = []
y_train_se = []
y_test_s1 = []
y_test_s2 = []
y_test_se = []

quit()

